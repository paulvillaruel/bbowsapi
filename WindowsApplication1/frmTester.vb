﻿Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Xml

Public Class frmTester



    Private Sub btnTestCreateBOL_Click(sender As Object, e As EventArgs) Handles btnTestCreateBOL.Click

        Dim soapClient As New ManifestServiceReference.ManifestSoapClient
        Dim result As String = ""

        Dim bolNbr As String = "TESTBOL000003"
        Dim vesselVisit As String = "KOHD1904"
        Dim consignee As String = "PAUL JEREMY VILLARUEL"
        Dim notifier As String = "MANILA PHILIPPINES"
        Dim dst As String = "SOBBO"
        Dim org As String = "SAJED"
        Dim cargoType As String = "C"
        Dim loadStatus As String = "F"
        Dim tradeMode As String = "DOMESTIC" 'if foreign, "FOREIGN"
        Dim pol As String = "SAJED"
        Dim pod As String = "SOBBO"

        Dim datasetBOL As New DataSet
        Dim tableContainer As New DataTable("Container")
        Dim tableCargo As New DataTable("Cargo")

        With tableContainer.Columns
            .Add("BOL", GetType(String))
            .Add("ContainerId", GetType(String))
            .Add("Quantity", GetType(Long))
            .Add("Volume", GetType(Decimal))
            .Add("Weight", GetType(Decimal))
            .Add("Seal01", GetType(String))
            .Add("Seal02", GetType(String))
            .Add("Seal03", GetType(String))
            .Add("CargoType", GetType(String))
        End With

        tableContainer.Rows.Add(bolNbr, "BMOU2551703", 2100, 32760.0, 40.0, "X0749088", "", "", "F")
        tableContainer.Rows.Add(bolNbr, "FCIU3751242", 2100, 32760.0, 40.0, "X0749089", "", "", "F")

        With tableCargo.Columns
            .Add("BOL", GetType(String))
            .Add("CustomTariffID", GetType(String))
            .Add("HSCode", GetType(String))
            .Add("CargoDescription", GetType(String))
            .Add("TotalQuantity", GetType(Long))
            .Add("PackType", GetType(String))
            .Add("TotalVolume", GetType(Decimal))
            .Add("VolumeUOM", GetType(String))
            .Add("TotalWeight", GetType(Decimal))
            .Add("WeightUOM", GetType(String))
            .Add("MarksNumbers", GetType(String))
        End With

        Dim cargoCode As String
        Dim cargoDescription As String

        cargoCode = "GENE" ' cargoCode = "CIG" if cigarettes
        cargoDescription = "GENERIC" ' cargoDescription = "CIGARETTES"

        tableCargo.Rows.Add(bolNbr, cargoCode, cargoDescription, "STC: TOTALLY 2100 CARTONS OF GHAMDAN CIGARETTES. NET WT: 22050 KGS. GROSS WT: 32760 KGS. CY/CY Shipper?s Load Stow Weigh & Count",
2100, "CNT", 40.0, "CBM", 32760.0, "KGM", "UNKNOWN")

        datasetBOL.Tables.Add(tableContainer)
        datasetBOL.Tables.Add(tableCargo)

        result = soapClient.PostManifestBillOfLading(bolNbr, vesselVisit, consignee, notifier, dst, cargoType, loadStatus, tradeMode, org,
                                            pol, pod, datasetBOL)

        Dim reader As New StringReader(result.ToString)
        Dim ds As New DataSet
        ds.ReadXml(reader)
        gridViewTest.DataSource = ds.Tables(0)
        gridViewTest.Refresh()

    End Sub

    Private Sub frmTester_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnVesselVisitEMDS_Click(sender As Object, e As EventArgs) Handles btnVesselVisitEMDS.Click

        Dim soapClient As New ManifestServiceReference.ManifestSoapClient
        Dim result As String = ""
        result = soapClient.GetEMDSVesselVisit.ToString()

        Dim reader As New StringReader(result)
        Dim ds As New DataSet
        ds.ReadXml(reader)
        gridViewTest.DataSource = ds.Tables(0)
        gridViewTest.Refresh()

    End Sub

    '
    ' BOL container / cargo
    '

    Dim dtUniqueBOLS As DataTable

    Private Sub btnGetUniqueBOLS_Click(sender As Object, e As EventArgs) Handles btnGetUniqueBOLS.Click

        Dim connectionString, sql As String
        connectionString = getConnectionString("staging")
        'sql = "select distinct(BOL) [BOL] from [dbo].[CUSTBOLCNT]"
        sql = "select TOP 5 * from (select distinct(BOL) from [dbo].[CUSTBOLCNT]) A"
        dtUniqueBOLS = getTable(sql, CommandType.Text)
        gridViewTest.DataSource = dtUniqueBOLS
        gridViewTest.Refresh()
        Me.Text = "Total rows: " & dtUniqueBOLS.Rows.Count

    End Sub

    Private m_connectionString As String = ""
    Public Function getConnectionString(dbtype As String) As String

        Select Case dbtype
            Case "staging"
                m_connectionString = My.Settings.staging'ConfigurationManager.ConnectionStrings("pts_staging_connection").ConnectionString
            Case "main"
                m_connectionString = ConfigurationManager.ConnectionStrings("pts_main_connection").ConnectionString
        End Select

        Return m_connectionString

    End Function

    Public Function getTable(
       ByVal commandText As String,
       ByVal commtype As CommandType,
       ByVal ParamArray params As System.Data.SqlClient.SqlParameter()) _
               As System.Data.DataTable

        Try


            Dim connection As New System.Data.SqlClient.SqlConnection(m_connectionString)
            Dim dataAdapter As New System.Data.SqlClient.SqlDataAdapter
            Dim command As New System.Data.SqlClient.SqlCommand(commandText)
            Dim dataTable As New DataTable

            connection.Open()
            command.Connection = connection
            command.CommandType = commtype

            command.Parameters.AddRange(params)
            dataAdapter.SelectCommand = command
            dataAdapter.Fill(dataTable)
            connection.Close()

            If Not IsNothing(dataAdapter) Then dataAdapter.Dispose()
            If Not IsNothing(command) Then command.Dispose()
            If Not IsNothing(connection) Then connection.Dispose()

            dataAdapter = Nothing
            command = Nothing
            connection = Nothing

            Return dataTable

        Catch ex As Exception

            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)
            Return tableException

        End Try

    End Function

    Private Sub btnSaveBOLCargo_Click(sender As Object, e As EventArgs) Handles btnSaveBOLCargo.Click

        Application.DoEvents()

        Dim soapClient As New RemoteCustomsServices.CustomsServiceSoapClient
        Dim soapResponse As RemoteCustomsServices.CustomsEntryResponse
        Dim bolString As String = ""
        Dim resultString As String = ""

        For Each dr As DataRow In dtUniqueBOLS.Rows
            bolString = dr.Item("BOL")
            soapResponse = soapClient.GetCustomsEntry(bolString, "dpworld", "k23@351")
            resultString = ""
            updateCargoItem(soapResponse.ToString(), bolString)
        Next

    End Sub

    Private Sub updateCargoItem(xml As String, bol As String)

        Dim ds As New DataSet
        ds.ReadXml(xml)

        For Each drItem As DataRow In ds.Tables("containerInfo").Rows

            getTable("insert_Customs_CargoItem", CommandType.StoredProcedure,
                     New SqlClient.SqlParameter("@BOL", bol),
                     New SqlClient.SqlParameter("@HSCODE", drItem.Item("hsCode")),
                     New SqlClient.SqlParameter("@DESCRIPTION", drItem.Item("description")),
                     New SqlClient.SqlParameter("@QUANTITY", drItem.Item("quantity")),
                     New SqlClient.SqlParameter("@PACKAGETYPE", drItem.Item("packageType")))

        Next

    End Sub


End Class
