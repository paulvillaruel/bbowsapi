﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTester
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnTestCreateBOL = New System.Windows.Forms.Button()
        Me.btnVesselVisitEMDS = New System.Windows.Forms.Button()
        Me.gridViewTest = New System.Windows.Forms.DataGridView()
        Me.btnGetUniqueBOLS = New System.Windows.Forms.Button()
        Me.btnSaveBOLCargo = New System.Windows.Forms.Button()
        CType(Me.gridViewTest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnTestCreateBOL
        '
        Me.btnTestCreateBOL.Location = New System.Drawing.Point(829, 32)
        Me.btnTestCreateBOL.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnTestCreateBOL.Name = "btnTestCreateBOL"
        Me.btnTestCreateBOL.Size = New System.Drawing.Size(255, 79)
        Me.btnTestCreateBOL.TabIndex = 0
        Me.btnTestCreateBOL.Text = "Test Create BOL"
        Me.btnTestCreateBOL.UseVisualStyleBackColor = True
        '
        'btnVesselVisitEMDS
        '
        Me.btnVesselVisitEMDS.Location = New System.Drawing.Point(1091, 32)
        Me.btnVesselVisitEMDS.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnVesselVisitEMDS.Name = "btnVesselVisitEMDS"
        Me.btnVesselVisitEMDS.Size = New System.Drawing.Size(255, 79)
        Me.btnVesselVisitEMDS.TabIndex = 1
        Me.btnVesselVisitEMDS.Text = "Test Get Vessel Visit EMDS"
        Me.btnVesselVisitEMDS.UseVisualStyleBackColor = True
        '
        'gridViewTest
        '
        Me.gridViewTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridViewTest.Location = New System.Drawing.Point(14, 121)
        Me.gridViewTest.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.gridViewTest.Name = "gridViewTest"
        Me.gridViewTest.RowTemplate.Height = 24
        Me.gridViewTest.Size = New System.Drawing.Size(1333, 596)
        Me.gridViewTest.TabIndex = 2
        '
        'btnGetUniqueBOLS
        '
        Me.btnGetUniqueBOLS.Location = New System.Drawing.Point(12, 13)
        Me.btnGetUniqueBOLS.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGetUniqueBOLS.Name = "btnGetUniqueBOLS"
        Me.btnGetUniqueBOLS.Size = New System.Drawing.Size(255, 79)
        Me.btnGetUniqueBOLS.TabIndex = 3
        Me.btnGetUniqueBOLS.Text = "Get Unique BOLS"
        Me.btnGetUniqueBOLS.UseVisualStyleBackColor = True
        '
        'btnSaveBOLCargo
        '
        Me.btnSaveBOLCargo.Location = New System.Drawing.Point(273, 13)
        Me.btnSaveBOLCargo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSaveBOLCargo.Name = "btnSaveBOLCargo"
        Me.btnSaveBOLCargo.Size = New System.Drawing.Size(255, 79)
        Me.btnSaveBOLCargo.TabIndex = 3
        Me.btnSaveBOLCargo.Text = "Save BOL Cargo Items"
        Me.btnSaveBOLCargo.UseVisualStyleBackColor = True
        '
        'frmTester
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1357, 736)
        Me.Controls.Add(Me.btnSaveBOLCargo)
        Me.Controls.Add(Me.btnGetUniqueBOLS)
        Me.Controls.Add(Me.gridViewTest)
        Me.Controls.Add(Me.btnVesselVisitEMDS)
        Me.Controls.Add(Me.btnTestCreateBOL)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmTester"
        Me.Text = "Form1"
        CType(Me.gridViewTest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnTestCreateBOL As Button
    Friend WithEvents btnVesselVisitEMDS As Button
    Friend WithEvents gridViewTest As DataGridView
    Friend WithEvents btnGetUniqueBOLS As Button
    Friend WithEvents btnSaveBOLCargo As Button
End Class
