﻿Imports System.Data
Imports SQLDAL
Imports System.Text.RegularExpressions
Imports System.Configuration

Public Class IntegrationController

    Public Enum CustomListType
        DISCHARGE
        LOAD
        VESSELS
        VISITS
        BOOKINGSTATUS
    End Enum

    Private m_connectionString As String

    Public Function getConnectionString(dbtype As String) As String

        Select Case dbtype
            Case "staging"
                m_connectionString = ConfigurationManager.ConnectionStrings("pts_staging_connection").ConnectionString
            Case "main"
                m_connectionString = ConfigurationManager.ConnectionStrings("pts_main_connection").ConnectionString
            Case "cwa"
                m_connectionString = ConfigurationManager.ConnectionStrings("cwa_connection").ConnectionString
        End Select

        Return m_connectionString

    End Function

    Public Function getTable(
       ByVal commandText As String,
       ByVal commtype As CommandType,
       ByVal ParamArray params As System.Data.SqlClient.SqlParameter()) _
               As System.Data.DataTable

        Try


            Dim connection As New System.Data.SqlClient.SqlConnection(m_connectionString)
            Dim dataAdapter As New System.Data.SqlClient.SqlDataAdapter
            Dim command As New System.Data.SqlClient.SqlCommand(commandText)
            Dim dataTable As New DataTable

            connection.Open()
            command.Connection = connection
            command.CommandType = commtype

            command.Parameters.AddRange(params)
            dataAdapter.SelectCommand = command
            dataAdapter.Fill(dataTable)
            connection.Close()

            If Not IsNothing(dataAdapter) Then dataAdapter.Dispose()
            If Not IsNothing(command) Then command.Dispose()
            If Not IsNothing(connection) Then connection.Dispose()

            dataAdapter = Nothing
            command = Nothing
            connection = Nothing

            Return dataTable

        Catch ex As Exception

            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)

            Global_asax.logger.Fatal("SQL getTable fatal error. Parameters: " & "{commandText, commtype, params()}, Values: " &
                                                      "{" & commandText & "," & commtype & "," & params.ToArray.ToString & "}", ex)

            Return tableException

        End Try

    End Function

    Public Function isDataValidated(inputData As String, infoType As String) As Boolean
        Dim regex As Regex
        Dim stringPattern As String = ""

        Select Case infoType
            Case "container" : stringPattern = ConfigurationManager.AppSettings("regex_container")
            Case "release_order_no" : stringPattern = ConfigurationManager.AppSettings("regex_release_order_no")
            Case "bol_no" : stringPattern = ConfigurationManager.AppSettings("bol_no")
            Case "release_date_time" : stringPattern = ConfigurationManager.AppSettings("regex_release_date_time") '20.10.2003 08:10
            Case "releasing_officer" : stringPattern = ConfigurationManager.AppSettings("regex_releasing_officer")
            Case "entry_no" : stringPattern = ConfigurationManager.AppSettings("regex_entry_no")
            Case "validity_date" : stringPattern = ConfigurationManager.AppSettings("regex_validity_date") '31.12.6008
            Case "user_id" : stringPattern = ConfigurationManager.AppSettings("regex_user_id")
            Case "filter_date_time" : stringPattern = ConfigurationManager.AppSettings("regex_filter_date_time")
            Case "valid_token_guid" : stringPattern = ConfigurationManager.AppSettings("regex_valid_token_guid")
        End Select

        If stringPattern = "" Then
            Return True
        Else
            regex = New Regex(stringPattern)
            Dim match As Match = regex.Match(inputData)
            If match.Success Then
                Return True
            Else

                Global_asax.logger.Warn("isDataValidated - mismatch. Parameters: " & "{inputData, infoType}, Values: " &
                                                      "{" & inputData & "," & infoType & "*" & stringPattern & "}")

                Return False
            End If
        End If


    End Function

    Public Function getSettingValue(keyName As String) As String
        Return ConfigurationManager.AppSettings(keyName).ToString
    End Function





End Class
