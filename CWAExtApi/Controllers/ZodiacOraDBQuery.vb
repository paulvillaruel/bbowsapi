﻿Imports Microsoft.VisualBasic
Imports System.IO

Imports System.Data
Imports Oracle.ManagedDataAccess.Client

Public Class ZodiacOraDBQuery

    Public Enum CustomListType
        DISCHARGE
        LOAD
        VESSELS
        VISITS
        BOOKINGSTATUS
        DWELLTIMEDETAILS
    End Enum
    Dim m_connectionString As String

    Public Function getConnectionString(ByVal dbtype As String) As String

        Select Case dbtype
            Case "zodiac"
                m_connectionString = ConfigurationManager.ConnectionStrings("zodiac").ConnectionString
            Case "zodiac_tms"
                m_connectionString = ConfigurationManager.ConnectionStrings("zodiac_tms").ConnectionString
            Case Else
                m_connectionString = ""
                Global_asax.logger.Warn("Oracle DB warning -  connectionString not set.")
        End Select

        Return m_connectionString

    End Function

    Public Function isDataValidated(inputData As String, infoType As String) As Boolean
        Dim regex As Regex
        Dim stringPattern As String = ""

        Select Case infoType
            Case "container" : stringPattern = ConfigurationManager.AppSettings("regex_container")
            Case "release_order_no" : stringPattern = ConfigurationManager.AppSettings("regex_release_order_no")
            Case "bol_no" : stringPattern = ConfigurationManager.AppSettings("bol_no")
            Case "release_date_time" : stringPattern = ConfigurationManager.AppSettings("regex_release_date_time") '20.10.2003 08:10
            Case "releasing_officer" : stringPattern = ConfigurationManager.AppSettings("regex_releasing_officer")
            Case "entry_no" : stringPattern = ConfigurationManager.AppSettings("regex_entry_no")
            Case "validity_date" : stringPattern = ConfigurationManager.AppSettings("regex_validity_date") '31.12.6008
            Case "user_id" : stringPattern = ConfigurationManager.AppSettings("regex_user_id")
            Case "filter_date_time" : stringPattern = ConfigurationManager.AppSettings("regex_filter_date_time")
            Case "valid_token_guid" : stringPattern = ConfigurationManager.AppSettings("regex_valid_token_guid")
        End Select

        If stringPattern = "" Then
            Return True
        Else
            regex = New Regex(stringPattern)
            Dim match As Match = regex.Match(inputData)
            If match.Success Then
                Return True
            Else
                Return False
            End If
        End If


    End Function

    '
    ' Get Discharge List 
    ' Input: DateFrom, DateTo
    '
    Public Function getContainerDischargeList(ByVal dateFrom As String, ByVal dateTo As String) As DataTable
        Dim tableDischargeList As New DataTable
        tableDischargeList = getList(CustomListType.DISCHARGE, dateFrom, dateTo)
        tableDischargeList.TableName = "DischargeList"
        Return tableDischargeList
    End Function

    '
    ' Get Load List 
    ' Input: DateFrom, DateTo
    '
    Public Function getContainerLoadList(ByVal dateFrom As String, ByVal dateTo As String) As DataTable
        Dim tableLoadList As New DataTable
        tableLoadList = getList(CustomListType.LOAD, dateFrom, dateTo)
        tableLoadList.TableName = "LoadList"
        Return tableLoadList
    End Function

    '
    ' Get Vessel
    '
    '
    Public Function getVesselList() As DataTable
        Dim tableVesselList As New DataTable
        tableVesselList = getList(CustomListType.VESSELS)
        tableVesselList.TableName = "VesselList"
        Return tableVesselList
    End Function

    '
    ' Get Vessel List
    '
    Public Function getVesseVisitList(ByVal dateFrom As String, ByVal dateTo As String) As DataTable
        Dim tableVesselVisitList As New DataTable
        tableVesselVisitList = getList(CustomListType.VISITS, dateFrom, dateTo)
        tableVesselVisitList.TableName = "VesselVisit"
        Return tableVesselVisitList
    End Function

    '
    ' Get Dwell Time List 
    ' Input: DateFrom, DateTo
    '
    Public Function getDwellTimeList(ByVal dateFrom As String, ByVal dateTo As String) As DataTable
        Dim tableDwell As New DataTable
        tableDwell = getList(CustomListType.DWELLTIMEDETAILS, dateFrom, dateTo)
        tableDwell.TableName = "DwellTimeList"
        Return tableDwell
    End Function

    Private Function returnErrorAsDataTable(retMsg As String) As DataTable

        Dim dataTableError As New DataTable
        dataTableError.Columns.Add("retmsg")
        dataTableError.Rows.Add(retMsg)
        Return dataTableError

    End Function

    Private Function getList(ByVal listType As CustomListType, Optional ByVal dateFrom As String = "",
                         Optional ByVal dateTo As String = "") As DataTable

        Try

            Dim retMsg As String = ""
            Dim dataListCustoms As New DataTable

            Select Case listType

                Case CustomListType.DISCHARGE

                    If Not isDataValidated(dateFrom, "filter_date_time") Then
                        retMsg = "-12:Invalid filter date. Must be 'yyyy-MM-dd HH:mm':InvalidInputException"
                        Return returnErrorAsDataTable(retMsg)
                    End If

                    If Not isDataValidated(dateTo, "filter_date_time") Then
                        retMsg = "-12:Invalid filter date. Must be 'yyyy-MM-dd HH:mm':InvalidInputException"
                        Return returnErrorAsDataTable(retMsg)
                    End If

                Case CustomListType.LOAD

                    If Not isDataValidated(dateFrom, "filter_date_time") Then
                        retMsg = "-12:Invalid filter date. Must be 'yyyy-MM-dd HH:mm':InvalidInputException"
                        Return returnErrorAsDataTable(retMsg)
                    End If

                    If Not isDataValidated(dateTo, "filter_date_time") Then
                        retMsg = "-12:Invalid filter date. Must be 'yyyy-MM-dd HH:mm':InvalidInputException"
                        Return returnErrorAsDataTable(retMsg)
                    End If

                Case CustomListType.VESSELS

                Case CustomListType.DWELLTIMEDETAILS

                    If Not isDataValidated(dateFrom, "filter_date_time") Then
                        retMsg = "-12:Invalid filter date. Must be 'yyyy-MM-dd HH:mm':InvalidInputException"
                        Return returnErrorAsDataTable(retMsg)
                    End If

                    If Not isDataValidated(dateTo, "filter_date_time") Then
                        retMsg = "-12:Invalid filter date. Must be 'yyyy-MM-dd HH:mm':InvalidInputException"
                        Return returnErrorAsDataTable(retMsg)
                    End If


            End Select


            m_connectionString = getConnectionString("zodiac")

            If listType = CustomListType.DWELLTIMEDETAILS Then
                dataListCustoms = getTable("MIS_COMPUTEDWELLTIME", CommandType.StoredProcedure,
                                                 New OracleParameter("dateFrom", dateFrom),
                                                 New OracleParameter("dateTo", dateTo),
                                                 New OracleParameter("cur", OracleDbType.RefCursor, ParameterDirection.Output))


            Else

                dataListCustoms = getTable("customs_getList", CommandType.StoredProcedure,
                                                 New OracleParameter("listType", listType.ToString),
                                                 New OracleParameter("dateFrom", dateFrom),
                                                 New OracleParameter("dateTo", dateTo),
                                                 New OracleParameter("cur", OracleDbType.RefCursor, ParameterDirection.Output),
                                                 New OracleParameter("cur1", OracleDbType.RefCursor, ParameterDirection.Output),
                                                 New OracleParameter("cur2", OracleDbType.RefCursor, ParameterDirection.Output))



            End If


            If Not dataListCustoms Is Nothing Then
                Return dataListCustoms
            End If

        Catch ex As Exception
            Global_asax.logger.Error("Zodiac getList error.", ex)
        End Try

    End Function

    Private Function getTable(
   ByVal commandText As String,
   ByVal commtype As CommandType,
   ByVal ParamArray params As OracleParameter()) _
           As System.Data.DataTable

        Try
            m_connectionString = getConnectionString("zodiac")

            Dim connection As New OracleConnection(m_connectionString)
            Dim dataAdapter As New OracleDataAdapter
            Dim command As New OracleCommand(commandText)
            Dim dataTable As New DataTable

            connection.Open()
            command.Connection = connection
            command.CommandType = commtype

            command.Parameters.AddRange(params)
            dataAdapter.SelectCommand = command
            dataAdapter.Fill(dataTable)
            connection.Close()

            If Not IsNothing(dataAdapter) Then dataAdapter.Dispose()
            If Not IsNothing(command) Then command.Dispose()
            If Not IsNothing(connection) Then connection.Dispose()

            dataAdapter = Nothing
            command = Nothing
            connection = Nothing

            Return dataTable

        Catch ex As Exception


            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)

            Global_asax.logger.Error("Zodiac getTable error.", ex)

            Return tableException


        End Try
    End Function


    '
    ' getContainerInfo
    '

    Private Function getTableTMS(
   ByVal commandText As String,
   ByVal commtype As CommandType,
   ByVal ParamArray params As OracleParameter()) _
           As System.Data.DataTable

        Try
            m_connectionString = getConnectionString("zodiac_tms")

            Dim connection As New OracleConnection(m_connectionString)
            Dim dataAdapter As New OracleDataAdapter
            Dim command As New OracleCommand(commandText)
            Dim dataTable As New DataTable

            connection.Open()
            command.Connection = connection
            command.CommandType = commtype

            command.Parameters.AddRange(params)
            dataAdapter.SelectCommand = command
            dataAdapter.Fill(dataTable)
            connection.Close()

            If Not IsNothing(dataAdapter) Then dataAdapter.Dispose()
            If Not IsNothing(command) Then command.Dispose()
            If Not IsNothing(connection) Then connection.Dispose()

            dataAdapter = Nothing
            command = Nothing
            connection = Nothing

            Return dataTable

        Catch ex As Exception


            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)

            Global_asax.logger.Error("Zodiac getTable error.", ex)

            Return tableException


        End Try
    End Function

    'todo:

    Public Function getContainerInfoTMS(parameterName As String, parameterValue As String) As DataTable

        Dim helper As New IntegrationHelper
        Dim sqlFindContainer As String = ""
        Dim dataContainerInfoTMS As New DataTable("ContainerList")
        Dim length As Integer
        Dim jsonFindContainer As String

        Try

            Select Case parameterName

                Case "BOL"

                    jsonFindContainer = helper.readJsonMessageTemplate("../RESTMessage/FindByBOL.json")
                    jsonFindContainer = jsonFindContainer.Replace("{", "")
                    jsonFindContainer = jsonFindContainer.Replace("}", "")
                    sqlFindContainer = jsonFindContainer.Split(":")(1).ToString()
                    sqlFindContainer = sqlFindContainer.Replace("@bol", parameterValue)
                    length = sqlFindContainer.Length
                    sqlFindContainer = Mid(sqlFindContainer, 3, (length - 3))
                    sqlFindContainer = sqlFindContainer.Replace(";""", "")
                    dataContainerInfoTMS = getTableTMS(sqlFindContainer, CommandType.Text)

                Case "CNT"

                    jsonFindContainer = helper.readJsonMessageTemplate("../RESTMessage/FindByContainerId.json")
                    jsonFindContainer = jsonFindContainer.Replace("{", "")
                    jsonFindContainer = jsonFindContainer.Replace("}", "")
                    sqlFindContainer = jsonFindContainer.Split(":")(1).ToString()
                    sqlFindContainer = sqlFindContainer.Replace("@containerId", parameterValue)
                    length = sqlFindContainer.Length
                    sqlFindContainer = Mid(sqlFindContainer, 3, (length - 3))
                    sqlFindContainer = sqlFindContainer.Replace(";""", "")
                    dataContainerInfoTMS = getTableTMS(sqlFindContainer, CommandType.Text)

            End Select


            If Not dataContainerInfoTMS Is Nothing Then
                Return dataContainerInfoTMS
            End If

        Catch ex As Exception

            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)
            Global_asax.logger.Error("Zodiac getTable error.", ex)

            Return tableException

        End Try

    End Function


    Public Function getVesselScheduleTMS() As DataTable

        Dim helper As New IntegrationHelper
        Dim sqlVesselSchedules As String = ""
        Dim dataVesselSchedules As New DataTable
        Dim elements As String()
        Dim length As Integer
        Try

            Dim jsonFindContainer = helper.readJsonMessageTemplate("../RESTMessage/InquiryVesselSchedules.json")
            jsonFindContainer = jsonFindContainer.Replace("{", "")
            jsonFindContainer = jsonFindContainer.Replace("}", "")
            elements = jsonFindContainer.Split("=")
            sqlVesselSchedules = elements(1)
            length = sqlVesselSchedules.Length
            sqlVesselSchedules = Mid(sqlVesselSchedules, 3, (length - 3))
            sqlVesselSchedules = sqlVesselSchedules.Replace("""", "")
            sqlVesselSchedules = sqlVesselSchedules.Replace(";""", "")
            dataVesselSchedules = getTableTMS(sqlVesselSchedules, CommandType.Text)

            If Not dataVesselSchedules Is Nothing Then
                Return dataVesselSchedules
            End If

        Catch ex As Exception

            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)
            Global_asax.logger.Error("Zodiac getTable error.", ex)

            Return tableException

        End Try

    End Function

    Public Function getVesselScheduleEMDS() As DataTable

        Dim helper As New IntegrationHelper
        Dim sqlVesselSchedules As String = ""
        Dim dataVesselSchedules As New DataTable

        Try

            Dim jsonFindContainer = helper.readJsonMessageTemplate("../RESTMessage/ManifestListVesselVisitID.json")
            jsonFindContainer = jsonFindContainer.Replace("{", "")
            jsonFindContainer = jsonFindContainer.Replace("}", "")
            sqlVesselSchedules = jsonFindContainer.Split(":")(1).ToString() & jsonFindContainer.Split(":")(2).ToString()
            Dim length As Integer = sqlVesselSchedules.Length
            sqlVesselSchedules = Mid(sqlVesselSchedules, 3, (length - 3))
            sqlVesselSchedules = sqlVesselSchedules.Replace("""", "")
            sqlVesselSchedules = sqlVesselSchedules.Replace(";""", "")
            dataVesselSchedules = getTableTMS(sqlVesselSchedules, CommandType.Text)

            If Not dataVesselSchedules Is Nothing Then
                Return dataVesselSchedules
            End If

        Catch ex As Exception

            Dim tableException As New DataTable()
            tableException.Columns.Add("retval", GetType(Integer))
            tableException.Columns.Add("retmsg", GetType(String))
            tableException.Rows.Add(2, ex.ToString)
            Global_asax.logger.Error("Zodiac getTable error.", ex)

            Return tableException

        End Try

    End Function

End Class
