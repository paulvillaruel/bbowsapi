﻿Imports System.Data
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.Data.Linq


Public Class InquiryController

    Dim oradbQuery As New ZodiacOraDBQuery


    '
    ' GetVesselSchedule
    '

    Public Function getVesselSchedule() As DataTable
        Dim tableVesselSchedule As DataTable
        Try
            tableVesselSchedule = oradbQuery.getVesselScheduleTMS()
            If tableVesselSchedule Is Nothing Then
                Return returnErrorAsDataTable("No vessel schedules found.")
            End If
            If tableVesselSchedule.Rows.Count = 0 Then
                Return returnErrorAsDataTable("No vessel schedules found.")
            End If
            oradbQuery = Nothing
            Return tableVesselSchedule
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return returnErrorAsDataTable("Vessel schedules listing encountered error. " & ex.Message)
        End Try

    End Function


    '
    ' Get Vessel Visit Schedule from PTS
    '

    Public Function getVesselSchedulePTS() As DataTable
        Dim integrationController As New IntegrationController
        integrationController.getConnectionString("main")
        Dim tableVesselSchedule As DataTable
        Try
            tableVesselSchedule = integrationController.getTable("cwa_getVesselScheduleRange", CommandType.StoredProcedure)
            integrationController = Nothing
            Return tableVesselSchedule
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return returnErrorAsDataTable("GC Vessel schedules listing encountered error. " & ex.Message)
        End Try

    End Function


    Public Function returnErrorAsDataTable(retMsg As String) As DataTable

        Dim dataTableError As New DataTable
        dataTableError.Columns.Add("retmsg")
        dataTableError.Rows.Add(retMsg)
        Return dataTableError

    End Function

    '
    ' GetCustomsEntry
    '

    Public Function getCustomsEntryBOL(billOfLading As String) As String
        Try

            Dim customsReference As New CustomsReference.CustomsServiceSoapClient
            Dim customsResponse As CustomsReference.CustomsEntryResponse
            Dim userId As String = ""
            Dim password As String = ""
            Dim result As String = ""

            userId = ConfigurationManager.AppSettings("customs_user_id")
            password = ConfigurationManager.AppSettings("customs_password")
            customsResponse = customsReference.GetCustomsEntry(billOfLading, userId, password)
            result = Newtonsoft.Json.JsonConvert.SerializeObject(customsResponse)

            customsReference = Nothing
            customsResponse = Nothing

            Return result

        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return ex.ToString()
        End Try

    End Function


    '
    'Container Inquiry
    '
    Public Function inquireContainerList(parameterName As String, parameterValue As String) As DataTable
        Dim tableContainerList As New DataTable
        Dim tableCType As New DataTable
        Dim tableZones As New DataTable

        Try

            Dim oraDBQuery As New ZodiacOraDBQuery
            tableContainerList = oraDBQuery.getContainerInfoTMS(parameterName, parameterValue)

            If Not tableContainerList Is Nothing Then

                If tableContainerList.Rows.Count <> 0 Then

                    tableCType = getCType()
                    tableZones = getZoneDescriptionList()

                    If tableCType Is Nothing Then
                        tableContainerList = returnErrorAsDataTable("CTypes not found.")
                    End If

                    If tableCType.Rows.Count = 0 Then
                        tableContainerList = returnErrorAsDataTable("CTypes list is empty.")
                    End If

                    Dim tableCombined As New DataTable
                    tableCombined = mergeTables(tableContainerList, tableCType, tableZones)
                    tableCombined.TableName = "ContainerList"
                    oraDBQuery = Nothing
                    tableContainerList = Nothing
                    tableCType = Nothing
                    Return tableCombined

                Else

                    oraDBQuery = Nothing
                    tableContainerList = returnErrorAsDataTable("Record not found.")
                    tableContainerList.TableName = "ContainerList"
                    Return tableContainerList

                End If

            Else

                oraDBQuery = Nothing
                tableContainerList = returnErrorAsDataTable("Container list is empty.")
                tableContainerList.TableName = "ContainerList"
                Return tableContainerList

            End If

        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return returnErrorAsDataTable("Inquiry inquireContainerList error. " & ex.Message)
        End Try

    End Function



    '
    'Container Inquiry
    '
    Public Function inquireContainerDetail(containerId As String) As DataTable

        Try

            Dim restZodiac As New RestSharpZodiacController
            Dim util As New Utils
            Dim dsContainerDetail As New DataSet
            Dim tableResult As New DataTable
            Dim resultCode As Integer = 0
            Dim restMessage As String = ""


            restMessage = restZodiac.inquiryGetContainerDetail(containerId)
            dsContainerDetail = util.readDataFromJson(restMessage)
            resultCode = Int32.Parse(dsContainerDetail.Tables("result").Rows(0).Item("resultCode"))

            If resultCode = 0 Then
                tableResult = returnErrorAsDataTable("Record not found.")
                Return tableResult
            Else
                tableResult = dsContainerDetail.Tables("cntrInfo")
            End If

            restZodiac = Nothing
            If tableResult Is Nothing Then
                oradbQuery = Nothing
                tableResult = returnErrorAsDataTable("Record not found.")
            End If
            If tableResult.Rows.Count = 0 Then
                oradbQuery = Nothing
                tableResult = returnErrorAsDataTable("Container detail is empty.")
            End If

            '
            ' Add "nextStep" column
            '
            Dim holdCodes As String = ""
            Dim ss As String = ""
            Dim nextStep As String = ""
            Dim nextStep_SO As String = ""
            Dim yardPosition As String = ""


            For Each rowResult As DataRow In tableResult.Rows
                holdCodes = rowResult.Item("holdCodes")
                ss = rowResult.Item("shippingStatusCode")
                yardPosition = rowResult.Item("bayPosn")
                Exit For
            Next

            Dim nextStepColumn As New DataColumn("nextStep", GetType(String))
            Dim nextStepColumn_SO As New DataColumn("nextStep_SO", GetType(String))

            If holdCodes = "" Then

                Select Case ss
                    Case "IM"
                        nextStepColumn.DefaultValue = "Proceed to Delivery/Stripping"
                        nextStepColumn_SO.DefaultValue = "Bixi Lacagta Labiska ama Strippinka"

                    Case "EX"
                        nextStepColumn.DefaultValue = "For Export Operations"
                        nextStepColumn_SO.DefaultValue = "For Export Operation"

                    Case Else
                        nextStepColumn.DefaultValue = "Check with Terminal"
                        nextStepColumn_SO.DefaultValue = "Ka hubi Jidka Terminalka"

                End Select

                tableResult.Columns.Add(nextStepColumn)
                tableResult.Columns.Add(nextStepColumn_SO)

            Else

                nextStep = getNextStep(holdCodes.Split(","), ss)
                nextStep_SO = getNextStep(holdCodes.Split(","), ss, "SO")

                nextStepColumn.DefaultValue = IIf(nextStep <> "", nextStep, "Check with Terminal")
                nextStepColumn_SO.DefaultValue = IIf(nextStep_SO <> "", nextStep_SO, "Ka hubi Jidka Terminalka")

                tableResult.Columns.Add(nextStepColumn)
                tableResult.Columns.Add(nextStepColumn_SO)

            End If

            '
            ' Add yard zone
            '
            Dim tableZones As DataTable
            tableZones = getZoneDescriptionList()

            Dim yardZone As New DataColumn("yardZone", GetType(String))
            yardZone.DefaultValue = getZoneDescriptionString("ContainerDetail", yardPosition, tableZones)

            tableResult.Columns.Add(yardZone)
            tableResult.TableName = "ContainerDetail"

            Return tableResult

        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return returnErrorAsDataTable("Inquiry inquireContainerDetail error. " & ex.Message)
        End Try

    End Function

    '
    ' Get Next Step
    '

    Public Function getNextStep(holdCodes() As String, ss As String, Optional lang As String = "") As String
        Dim integrationController As New IntegrationController
        integrationController.getConnectionString("cwa")
        Dim tableCwaNextStep As DataTable
        Dim nextStep As String = ""
        Try
            tableCwaNextStep = integrationController.getTable("getNextStep", CommandType.StoredProcedure,
                                                              New SqlClient.SqlParameter("@cType", ss))
            For Each rowCwaNextStep As DataRow In tableCwaNextStep.Rows
                If Len(rowCwaNextStep.Item("IOrder")) > 0 Then
                    If holdCodes.Contains(rowCwaNextStep.Item("Hold")) Then
                        Select Case lang
                            Case "" : nextStep = rowCwaNextStep.Item("NStep").ToString
                            Case "SO" : nextStep = rowCwaNextStep.Item("NStep_SO").ToString
                            Case Else : nextStep = ""
                        End Select
                        Exit For
                    End If
                End If
            Next
            integrationController = Nothing
            Return nextStep
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return "Next step error"
        End Try

    End Function

    '
    ' getCType
    '

    Public Function getCType() As DataTable
        Dim integrationController As New IntegrationController
        integrationController.getConnectionString("cwa")
        Dim tableCType As DataTable
        Dim nextStep As String = ""
        Try
            tableCType = integrationController.getTable("getCType", CommandType.StoredProcedure)
            integrationController = Nothing
            Return tableCType
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return Nothing
        End Try

    End Function


    Public Function getZoneDescriptionList() As DataTable
        Dim integrationController As New IntegrationController
        integrationController.getConnectionString("cwa")
        Dim tableZoneDescription As DataTable
        Dim nextStep As String = ""
        Try
            tableZoneDescription = integrationController.getTable("getZoneDescription", CommandType.StoredProcedure)
            integrationController = Nothing
            Return tableZoneDescription
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return Nothing
        End Try

    End Function

    Public Function getZoneDescriptionString(sourceReference As String, positionReference As String,
                                             zonesTable As DataTable) As String
        Dim blockId As String = ""
        Dim zones As DataRow()
        Dim zoneDescriptionString As String = ""

        Select Case sourceReference
            Case "ContainerList"
                If positionReference.Contains(".") Then
                    'SLOT
                    Dim position() As String
                    position = positionReference.Split(".")
                    blockId = Left(position(0).ToString, 3)
                Else
                    'HEAP
                    blockId = positionReference
                End If
                zones = zonesTable.Select("BLKID ='" & blockId & "'")
                For Each zone In zones
                    zoneDescriptionString = zone.Item("ZONEDS_EN")
                    Exit For
                Next
            Case "ContainerDetail"
                blockId = Left(positionReference, 3)
                zones = zonesTable.Select("BLKID ='" & blockId & "'")
                If zones.Count > 1 Then
                    For Each zone In zones
                        zoneDescriptionString = zone.Item("ZONEDS_EN")
                        Exit For
                    Next
                Else
                    zones = zonesTable.Select("BLKID ='" & positionReference & "'")
                    For Each zone In zones
                        zoneDescriptionString = zone.Item("ZONEDS_EN")
                        Exit For
                    Next
                End If
            Case Else
                zoneDescriptionString = "In Yard"
        End Select

        Return zoneDescriptionString

    End Function


    Private Function getCTypeString(cTypeTable As DataTable, type As String) As String
        Dim cTypeString As String = ""
        Dim typeRows As DataRow()
        typeRows = cTypeTable.Select("CType ='" & type & "'")
        For Each typeRow In typeRows
            cTypeString = typeRow.Item("CTypeShort")
            Exit For
        Next
        Return cTypeString
    End Function

    Private Function mergeTables(tableContainer As DataTable, tableCType As DataTable,
                                 tableZone As DataTable) As DataTable

        Dim tableMerged As New DataTable
        tableMerged.Columns.Add("ContainerId")
        tableMerged.Columns.Add("Size")
        tableMerged.Columns.Add("Type")
        tableMerged.Columns.Add("Hgt")
        tableMerged.Columns.Add("FE")
        tableMerged.Columns.Add("BOL")
        tableMerged.Columns.Add("POL")
        tableMerged.Columns.Add("POD")
        tableMerged.Columns.Add("Location")
        tableMerged.Columns.Add("InDate")
        tableMerged.Columns.Add("Age")
        tableMerged.Columns.Add("SS")
        tableMerged.Columns.Add("TypeDescription")


        Dim cTypeString As String = ""
        Dim cZone As String = ""
        Dim location As String = ""

        For Each rowContainer As DataRow In tableContainer.Rows

            cTypeString = getCTypeString(tableCType, rowContainer.Item("Type").ToString)

            If rowContainer.Item("Location").ToString = "In-Yard" Then
                cZone = getZoneDescriptionString("ContainerList",
                                                 rowContainer.Item("CurrentPosition").ToString, tableZone)
                location = cZone
            Else
                location = rowContainer.Item("Location").ToString
            End If

            tableMerged.Rows.Add(rowContainer.Item("ContainerId"), rowContainer.Item("Size"),
                                 rowContainer.Item("Type"), rowContainer.Item("Hgt"),
                                 rowContainer.Item("FE"), rowContainer.Item("BOL"),
                                 rowContainer.Item("POL"), rowContainer.Item("POD"),
                                 location, rowContainer.Item("InDate"),
                                 rowContainer.Item("Age"), rowContainer.Item("SS"),
                                 cTypeString)
        Next

        Return tableMerged

    End Function


End Class
