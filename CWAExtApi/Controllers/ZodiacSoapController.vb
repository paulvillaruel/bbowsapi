﻿Imports System.Data
Imports System.IO
Imports System.Xml
Imports Newtonsoft

Public Class ZodiacSoapController

    Dim zodiacService As New CWAExtApiWS.ZodiacReference.ZodiacServiceSoapClient
    Dim helper As New IntegrationHelper

    Public Enum ZodiacServiceNames
        SECURITY_SVC
        MASTER_DATA_SVC
        CUSTOMER_PROFILE_SVC
        CHARGE_QUERY_SVC
        CNTR_UPDATE_WEB
    End Enum


    Public Function getAuthToken(userId As String, password As String, entityName As String) As String

        Dim token As String = ""

        Try

            Dim authServiceName As String = ZodiacServiceNames.SECURITY_SVC.ToString
            Dim authMessageType = "Authenticate"
            Dim authMessage = helper.readSoapMessageTemplate("../SoapMessage/Authenticate.xml")
            Dim authResponse As String = ""

            authMessage = authMessage.Replace("@transactionDate", DateTime.Now.ToString("yyyyMMddHHmmss"))
            authMessage = authMessage.Replace("@locale", ConfigurationManager.AppSettings("zodiac_locale"))
            authMessage = authMessage.Replace("@company", entityName)
            authMessage = authMessage.Replace("@ipAddress", helper.getIPAddress)
            authMessage = authMessage.Replace("@userId", userId)
            authMessage = authMessage.Replace("@company", entityName)
            authMessage = authMessage.Replace("@password", password)

            authResponse = zodiacService.serviceRequestWithMessageType(authServiceName, authMessageType, authMessage)

            Dim doc As XmlDocument = New XmlDocument
            doc.LoadXml(authResponse)
            Dim nodes As XmlNodeList = doc.SelectNodes("response/msg_body")
            token = nodes(0).InnerText

        Catch ex As Exception
            token = "INVALID_TOKEN_EXCEPTION" & ":" & ex.Message
            Global_asax.logger.Error("SOAP getAuth - INVALID_TOKEN_EXCEPTION. Parameters: " & "{userId, password, entityName}, Values: " &
                                             "{" & userId & "," & password & "," & entityName & "}", ex)

        End Try

        Return token



    End Function



    Public Function updateDOValidity(userId As String, password As String, containerId As String,
                                     doNum As String, doValidityDateTime As String) As String
        Dim updateDOResult As String = ""

        Try
            Dim authServiceName As String = ZodiacServiceNames.CNTR_UPDATE_WEB.ToString
            Dim authMessageType = "CNTR_UPDATE_REQ"
            Dim authMessage = helper.readSoapMessageTemplate("SoapMessage/ContainerUpdateWeb.xml")
            Dim authResponse As String = ""


            authMessage = authMessage.Replace("@user", userId)
            authMessage = authMessage.Replace("@locale", ConfigurationManager.AppSettings("zodiac_locale"))
            authMessage = authMessage.Replace("@transactionDate", DateTime.Now.ToString("yyyyMMddHHmmss"))
            authMessage = authMessage.Replace("@containerId", containerId)
            authMessage = authMessage.Replace("@doNum", doNum)

            doValidityDateTime = doValidityDateTime.Replace("-", "")
            doValidityDateTime = doValidityDateTime & "000000"

            authMessage = authMessage.Replace("@doValidityDateTime", doValidityDateTime)
            authResponse = zodiacService.serviceRequestWithMessageType(authServiceName, authMessageType, authMessage)

            Dim doc As XmlDocument = New XmlDocument
            doc.LoadXml(authResponse)
            Dim nodes As XmlNodeList = doc.SelectNodes("response/msg_body")
            updateDOResult = nodes(0).InnerText
            Return updateDOResult

        Catch ex As Exception

            updateDOResult = ex.Message

            Global_asax.logger.Error("SOAP updateDOValidity - ERROR. Parameters: " & "{userId, password, containerId, doNum, doValidityDateTime}, Values: " &
                                             "{" & userId & "," & password & "," & containerId &
                                             doNum & "," & doValidityDateTime & "}", ex)


        End Try

        Return updateDOResult

    End Function


    '
    ' Release container
    '

End Class
