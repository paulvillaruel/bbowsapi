﻿Imports System.Data
Imports RestSharp
Imports Newtonsoft
Imports Newtonsoft.Json


Public Class ManifestController

    Dim helper As New IntegrationHelper
    Dim zodiacSoapController As New ZodiacSoapController
    Dim integrationController As New IntegrationController
    Dim timeout As Integer = CInt(ConfigurationManager.AppSettings("rest_client_timeout"))

    '
    ' Post Manifest BOL
    '

    Public Class BOLObject
        Public Property bolNbr As String
        Public Property vesselVisitCode As String
        Public Property consignee As String
        Public Property notifier As String
        Public Property org As String
        Public Property pol As String
        Public Property pod As String
        Public Property dst As String
        Public Property cargoType As String
        Public Property loadStatus As String
        Public Property tradeMode As String
        Public Property user As String
        Public Property cargos As List(Of CargoItem)
        Public Property equipments As List(Of EquipmentItem)
    End Class

    Public Class CargoItem
        Public Property description As String
        Public Property commodityCode As String
        Public Property customCommodityClass As String
        Public Property qty As String
        Public Property qtyUom As String
        Public Property vol As String
        Public Property volUom As String
        Public Property weight As String
        Public Property weightUom As String
        Public Property eqpNbr As String
        Public Property marksNumbers As String
    End Class

    Public Class EquipmentItem
        Public Property eqpNbr As String
        Public Property seal1Nbr As String
        Public Property seal2Nbr As String
        Public Property seal3Nbr As String
    End Class

    Public Function createBOL(
                             bolNbr As String, vesselVisitCode As String,
                             consignee As String, notifier As String, dst As String, cargoType As String,
                             loadStatus As String, tradeMode As String,
                             tableCargoItemDetails As DataTable,
                             tableContainerItemDetails As DataTable, org As String,
                             pol As String, pod As String
                               ) As String

        Dim userId As String, password As String, entity As String

        userId = ConfigurationManager.AppSettings("zodiac_user_id")
        password = ConfigurationManager.AppSettings("zodiac_password")
        entity = ConfigurationManager.AppSettings("zodiac_entity")

        Try

            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/bols/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim requestMessage As String = ""
            Dim returnMessage As String = ""
            Dim equipmentNumberSelect As String = ""

            If integrationController.isDataValidated(token, "valid_token_guid") Then

                Dim client = New RestClient(invoiceURL)

                client.Timeout = timeout
                client.ReadWriteTimeout = timeout

                Dim request = New RestRequest("batch-create-bol", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("cache-control", "no-cache")
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.sessionToken", token)


                Dim bol As New BOLObject
                With bol
                    .bolNbr = bolNbr
                    .vesselVisitCode = vesselVisitCode
                    .consignee = consignee
                    .notifier = notifier
                    .org = org
                    .pol = pol
                    .pod = pod
                    .dst = dst
                    .cargoType = cargoType
                    .loadStatus = loadStatus
                    .tradeMode = tradeMode
                    .user = userId
                End With

                Dim cargoItem As CargoItem
                bol.cargos = New List(Of CargoItem)

                Dim equipmentItem As EquipmentItem
                bol.equipments = New List(Of EquipmentItem)


                Dim quantityInContainer As Double = 0.0
                Dim volumeInContainer As Double = 0.0
                Dim weightInContainer As Double = 0.0
                Dim count As Integer = 0

                '
                ' cargoLoop: per each cargo item found in the cargoReferenceObject
                '
                For Each cargoItemDetail As DataRow In tableCargoItemDetails.Rows
                    For Each equipmentItemRow As DataRow In tableContainerItemDetails.Rows
                        cargoItem = New CargoItem
                        With cargoItem

                            .marksNumbers = IIf(cargoItemDetail.Item("MarksNumbers") Is Nothing, "NA", CStr(cargoItemDetail.Item("MarksNumbers")))
                            .commodityCode = IIf(cargoItemDetail.Item("CustomTariffID") Is Nothing, "OTHERS", cargoItemDetail.Item("CustomTariffID"))
                            .customCommodityClass = IIf(cargoItemDetail.Item("HSCode") Is Nothing, "0000", cargoItemDetail.Item("HSCode"))
                            .description = IIf(cargoItemDetail.Item("CargoDescription") Is Nothing, "No description.", cargoItemDetail.Item("CargoDescription"))
                            .eqpNbr = equipmentItemRow.Item("ContainerId").ToString
                            .qty = equipmentItemRow.Item("Quantity").ToString
                            .qtyUom = IIf(cargoItemDetail.Item("PackType") Is Nothing, "NA", CStr(cargoItemDetail.Item("PackType")))
                            .vol = equipmentItemRow.Item("Volume").ToString
                            .volUom = IIf(cargoItemDetail.Item("VolumeUOM") Is Nothing, "NA", CStr(cargoItemDetail.Item("VolumeUOM")))
                            .weight = equipmentItemRow.Item("Weight").ToString
                            .weightUom = IIf(cargoItemDetail.Item("WeightUOM") Is Nothing, "NA", CStr(cargoItemDetail.Item("WeightUOM")))

                        End With
                        bol.cargos.Add(cargoItem)
                    Next
                Next


                '
                ' equipmentLoop: per each equipment item found in the equipmentReferenceObject
                ' container = equipmentItem object
                '
                For Each containerItemDetail As DataRow In tableContainerItemDetails.Rows
                    equipmentItem = New EquipmentItem
                    With equipmentItem
                        .eqpNbr = containerItemDetail.Item("ContainerId").ToString
                        .seal1Nbr = containerItemDetail.Item("Seal01").ToString
                        .seal2Nbr = containerItemDetail.Item("Seal02").ToString
                        .seal3Nbr = containerItemDetail.Item("Seal03").ToString
                    End With
                    bol.equipments.Add(equipmentItem)
                Next


                Dim bolToString As String = JsonConvert.SerializeObject(bol)
                bolToString = "[" & bolToString & "]"
                'requestMessage = JsonConvert.SerializeObject(bolToString)

                '
                ' Log insepection requestMessage
                '
                Global_asax.logger.Info(bolToString)

                request.AddParameter("application/json", bolToString, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)
                returnMessage = response.Content

            End If

            Return returnMessage

        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return ex.Message

        End Try

    End Function

    Public Function postManifestBillOfLading(
                             bolNbr As String, vesselVisitCode As String,
                             consignee As String, notifier As String, dst As String,
                             cargoType As String, loadStatus As String, tradeMode As String,
                             org As String, pol As String, pod As String, bolDataSet As DataSet) As DataTable

        Dim result As String = ""
        Dim tableContainer As DataTable
        Dim tableCargo As DataTable

        If bolDataSet Is Nothing Then Return returnMessage("-1", "BOL_DATASOURCE_NULL_EXCEPTION")

        tableContainer = bolDataSet.Tables("Container")
        If tableContainer Is Nothing Then Return returnMessage("-2", "BOL_CONTAINER_NULL_EXCEPTION")
        If tableContainer.Rows.Count = 0 Then Return returnMessage("-3", "BOL_CONTAINER_EMPTY_EXCEPTION")

        tableCargo = bolDataSet.Tables("Cargo")
        If tableCargo Is Nothing Then Return returnMessage("-2", "BOL_CARGO_NULL_EXCEPTION")
        If tableCargo.Rows.Count = 0 Then Return returnMessage("-3", "BOL_CARGO_EMPTY_EXCEPTION")

        If Len(bolNbr) = 0 Then Return returnMessage("-11", "BOL_NO_REQUIRED")
        If Len(consignee) = 0 Then Return returnMessage("-12", "CONSIGNEE_NO_REQUIRED")
        If Len(vesselVisitCode) = 0 Then Return returnMessage("-13", "VISIT_CODE_REQUIRED")
        If Len(dst) = 0 Then Return returnMessage("-14", "DESTINATION_REQUIRED")
        If Len(org) = 0 Then Return returnMessage("-15", "ORIGIN_REQUIRED")
        If Len(pol) = 0 Then Return returnMessage("-16", "POL_REQUIRED")
        If Len(pod) = 0 Then Return returnMessage("-17", "POD_REQUIRED")
        If Len(cargoType) = 0 Then Return returnMessage("-18", "CARGO_TYPE_REQUIRED")
        If Len(loadStatus) = 0 Then Return returnMessage("-19", "LOAD_STATUS_REQUIRED")
        If Len(tradeMode) = 0 Then Return returnMessage("-20", "TRADE_MODE_REQUIRED")


        result = createBOL(bolNbr, vesselVisitCode, consignee, notifier, dst, cargoType, loadStatus, tradeMode, tableCargo,
                           tableContainer, org, pol, pod)

        Return returnMessage("1", result)


    End Function

    Private Function returnMessage(code As String, message As String) As DataTable

        Dim tableException As New DataTable("result")
        tableException.Columns.Add("retval", GetType(String))
        tableException.Columns.Add("retmsg", GetType(String))
        tableException.Rows.Add(code, message)
        Return tableException

    End Function


    '
    ' Get Vessel Schedule EMDS
    '

    Public Function getVesselSchedule() As DataTable
        Dim oradbQuery As New ZodiacOraDBQuery
        Dim tableVesselSchedule As DataTable
        Try
            tableVesselSchedule = oradbQuery.getVesselScheduleEMDS()
            If tableVesselSchedule Is Nothing Then
                Return returnMessage("-1", "No vessel schedules found.")
            End If
            If tableVesselSchedule.Rows.Count = 0 Then
                Return returnMessage("-1", "No vessel schedules found.")
            End If
            oradbQuery = Nothing
            Return tableVesselSchedule
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            Return returnMessage("-1", "Vessel schedules listing encountered error. " & ex.Message)
        End Try
    End Function

End Class
