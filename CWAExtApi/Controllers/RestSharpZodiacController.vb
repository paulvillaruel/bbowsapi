﻿Imports RestSharp
Imports Newtonsoft
Imports Newtonsoft.Json

Public Class RestSharpZodiacController

    Private zodiacSoapController As New ZodiacSoapController
    Private integrationController As New IntegrationController
    Private helper As New IntegrationHelper
    Dim util As New Utils
    Dim timeout As Integer = CInt(ConfigurationManager.AppSettings("rest_client_timeout"))


    Public Function inquireInvoice(userId As String, password As String, entity As String,
                                   invoiceNumber As String) As String

        Try

            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/billing/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim returnMessage As String = ""


            If integrationController.isDataValidated(token, "valid_token_guid") Then

                invoiceURL = invoiceURL & "invoices/" & invoiceNumber

                Dim client = New RestClient(invoiceURL)
                client.Timeout = timeout
                client.ReadWriteTimeout = timeout

                Dim request = New RestRequest(Method.GET)
                request.AddHeader("cache-control", "no-cache")
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.sessionToken", token)
                Dim response As IRestResponse = client.Execute(request)

                returnMessage = response.Content

            End If

            Return returnMessage

        Catch ex As Exception
            Global_asax.logger.Fatal("RESTFul inquireInvoice fatal error. Parameters: " & "{userId, password, entity, invoiceNumber}, Values: " &
                                                 "{" & userId & "," & password & "," & entity & "," & invoiceNumber & "}", ex)
            Throw ex
        End Try



    End Function

    '
    ' Payment
    '

    Public Class PaymentObject
        Public Property code As String
        Public Property payments As List(Of PaymentItem)
    End Class

    Public Class PaymentItem
        Public Property paymentMethod() As String
        Public Property currency() As String
        Public Property amount() As Double
        Public Property bankAccount() As String
        Public Property paymentReference() As String
        Public Property paymentTm() As String
        Public Property exchangeRate() As Double
    End Class

    Public Function payInvoice(userId As String, password As String, entity As String,
                               invoiceNumber As String, paymentMethod As String, paymentCurrency As String,
                               exchangeRate As Double, amount As Double, bankAccount As String, paymentReference As String,
                                paymentTime As String) As String

        Try

            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/billing/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim requestMessage As String = ""
            Dim returnMessage As String = ""

            Dim creditNoteJsonSource As String = ""
            Dim creditNoteMessageDataSet As New DataSet
            Dim stringReader As IO.StringReader

            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                creditNoteJsonSource = inquireInvoice(userId, password, entity, invoiceNumber)
                Dim doc As New System.Xml.XmlDocument
                doc = JsonConvert.DeserializeXmlNode(creditNoteJsonSource, "root")
                stringReader = New IO.StringReader(doc.InnerXml)
                creditNoteMessageDataSet.ReadXml(stringReader)

                Dim client = New RestClient(invoiceURL)
                client.Timeout = timeout
                client.ReadWriteTimeout = timeout

                Dim request = New RestRequest("invoices/settle-invoice", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("z.sessionToken", token)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("cache-control", "no-cache")


                Dim paymentObject As New PaymentObject
                Dim paymentItem As New PaymentItem
                paymentObject.payments = New List(Of PaymentItem)
                paymentObject.code = invoiceNumber

                With paymentItem
                    .paymentMethod = paymentMethod
                    .currency = paymentCurrency
                    .amount = amount
                    .bankAccount = bankAccount
                    .paymentReference = paymentReference
                    .paymentTm = paymentTime
                    .exchangeRate = exchangeRate
                End With

                paymentObject.payments.Add(paymentItem)


                '
                ' Call inquireInvoice()
                ' return an array of creditNotes
                ' looop thru paymentObjectMobile.payments.Add(creditNote())
                '

                If Not creditNoteMessageDataSet.Tables("creditNotes") Is Nothing Then

                    For Each rowCredit As DataRow In creditNoteMessageDataSet.Tables("creditNotes").Rows
                        paymentItem = New PaymentItem

                        With paymentItem
                            .paymentMethod = "Credit Note"
                            .currency = rowCredit.Item("billCurrency")
                            .amount = Math.Abs(CDbl(rowCredit.Item("creditAmount")))
                            .bankAccount = ""
                            .paymentReference = rowCredit.Item("creditNoteNbr")
                            .paymentTm = paymentTime
                            .exchangeRate = exchangeRate
                        End With
                        paymentObject.payments.Add(paymentItem)
                    Next

                End If

                requestMessage = JsonConvert.SerializeObject(paymentObject)
                request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)

                returnMessage = response.Content

            End If

            Return returnMessage

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul payInvoice fatal error. Parameters: " & "{userId, password, entity, invoiceNumber, paymentMethod, paymentCurrency, exchangeRate, amount, bankAccount, paymentReference, paymentTime}, Values: " &
                                                 "{" & userId & "," & password & "," & entity & "," & invoiceNumber &
                                                 paymentMethod & "," & paymentCurrency & "," & exchangeRate & "," & amount & "," & bankAccount & "," & paymentReference & "," & paymentTime &
                                                    "}", ex)

            Throw ex

        End Try



    End Function

    '
    ' Bill of Lading
    '

    Public Class BOLObject
        Public Property bolNbr As String
        Public Property vesselVisitCode As String
        Public Property consignee As String
        Public Property org As String
        Public Property pol As String
        Public Property pod As String
        Public Property dst As String
        Public Property cargoType As String
        Public Property loadStatus As String
        Public Property tradeMode As String
        Public Property user As String
        Public Property cargos As List(Of CargoItem)
        Public Property equipments As List(Of EquipmentItem)
    End Class

    Public Class CargoItem
        Public Property description As String
        Public Property commodityCode As String
        Public Property customCommodityClass As String
        Public Property qty As String
        Public Property qtyUom As String
        Public Property vol As String
        Public Property volUom As String
        Public Property weight As String
        Public Property weightUom As String
        Public Property eqpNbr As String
    End Class

    Public Class EquipmentItem
        Public Property eqpNbr As String
        Public Property seal1Nbr As String
        Public Property seal3Nbr As String
    End Class


    'Public Class BillOfLading
    '    Public bolItems As IList(Of BOLObject)
    'End Class


    Public Function createBOL(userId As String, password As String, entity As String,
                              bolNbr As String, vesselVisitCode As String,
                              consignee As String, dst As String, cargoType As String,
                              loadStatus As String, tradeMode As String,
                              tableCargoItemDetails As DataTable,
                              tableContainerItemDetails As DataTable, org As String,
                              pol As String, pod As String
                                ) As String

        Try
            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/bols/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim requestMessage As String = ""
            Dim returnMessage As String = ""
            Dim equipmentNumberSelect As String = ""

            If integrationController.isDataValidated(token, "valid_token_guid") Then


                Dim client = New RestClient(invoiceURL)

                client.Timeout = timeout
                client.ReadWriteTimeout = timeout

                Dim request = New RestRequest("batch-create-bol", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("cache-control", "no-cache")
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.sessionToken", token)


                Dim bol As New BOLObject
                With bol
                    .bolNbr = bolNbr
                    .vesselVisitCode = vesselVisitCode
                    .consignee = consignee
                    .org = org
                    .pol = pol
                    .pod = pod
                    .dst = dst
                    .cargoType = cargoType
                    .loadStatus = loadStatus
                    .tradeMode = tradeMode
                    .user = userId
                End With

                Dim cargoItem As CargoItem
                bol.cargos = New List(Of CargoItem)

                Dim equipmentItem As EquipmentItem
                bol.equipments = New List(Of EquipmentItem)


                Dim quantityInContainer As Double = 0.0
                Dim volumeInContainer As Double = 0.0
                Dim weightInContainer As Double = 0.0
                Dim count As Integer = 0

                count = tableContainerItemDetails.Rows.Count()

                '
                ' cargoLoop: per each cargo item found in the cargoReferenceObject
                '
                For Each cargoItemDetail As DataRow In tableCargoItemDetails.Rows

                    quantityInContainer = Val(cargoItemDetail.Item("Quantity")) / count
                    volumeInContainer = Val(cargoItemDetail.Item("Volume")) / count
                    weightInContainer = Val(cargoItemDetail.Item("Weight")) / count

                    For Each equipmentItemRow As DataRow In tableContainerItemDetails.Rows
                        cargoItem = New CargoItem
                        With cargoItem
                            .commodityCode = IIf(cargoItemDetail.Item("CustomTariffID") Is Nothing, "0000", cargoItemDetail.Item("CustomTariffID"))
                            .customCommodityClass = IIf(cargoItemDetail.Item("HSCode") Is Nothing, "0000", cargoItemDetail.Item("HSCode"))
                            .description = IIf(cargoItemDetail.Item("CargoDescription") Is Nothing, "No description.", cargoItemDetail.Item("CargoDescription"))
                            .eqpNbr = equipmentItemRow.Item("ContainerId").ToString
                            .qty = quantityInContainer
                            .qtyUom = IIf(cargoItemDetail.Item("PackType") Is Nothing, "NA", CStr(cargoItemDetail.Item("PackType")))
                            .vol = volumeInContainer
                            .volUom = IIf(cargoItemDetail.Item("VolumeUOM") Is Nothing, "NA", CStr(cargoItemDetail.Item("VolumeUOM")))
                            .weight = weightInContainer
                            .weightUom = IIf(cargoItemDetail.Item("WeightUOM") Is Nothing, "NA", CStr(cargoItemDetail.Item("WeightUOM")))
                        End With
                        bol.cargos.Add(cargoItem)
                    Next
                Next


                '
                ' equipmentLoop: per each equipment item found in the equipmentReferenceObject
                ' container = equipmentItem object
                '
                For Each containerItemDetail As DataRow In tableContainerItemDetails.Rows
                    equipmentItem = New EquipmentItem
                    With equipmentItem
                        .eqpNbr = containerItemDetail.Item("ContainerId").ToString
                        .seal1Nbr = containerItemDetail.Item("Seal01").ToString
                        .seal3Nbr = containerItemDetail.Item("Seal03").ToString
                    End With
                    bol.equipments.Add(equipmentItem)
                Next

                ' bols.bolItems.Add(bol)

                Dim bolToString As String = JsonConvert.SerializeObject(bol)
                bolToString = "[" & bolToString & "]"
                requestMessage = JsonConvert.SerializeObject(bolToString)
                'request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                request.AddParameter("application/json", bolToString, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)
                returnMessage = response.Content

            End If

            Return returnMessage

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul createBOL fatal error.", ex)
            Return ex.Message

            Throw ex

        End Try

    End Function

    '
    ' TODO: payInvoiceMobile
    '
    ' Classes
    ' Methods
    Public Class PaymentItemMobile
        Public Property paymentMethod() As String
        Public Property currency() As String
        Public Property exchangeRate() As Double
        Public Property amount() As Double
        Public Property bankAccount() As String
        Public Property paymentReference() As String
        Public Property bankTransReference() As String
        Public Property payByFlexField1() As String
        Public Property payByFlexField2() As String
        Public Property paymentTm() As String
    End Class

    Public Class PaymentMobileObject
        Public Property code As String
        Public Property payments As List(Of PaymentItemMobile)
    End Class

    Public Function payInvoiceMobile(userId As String, password As String, entity As String,
                               invoiceNumber As String, paymentCurrency As String,
                               exchangeRate As Double, amount As Double, bankAccount As String, ePaymentReference As String,
                               bankTransReference As String, subscriberName As String, subscriberMobile As String,
                               paymentTime As String) As String

        Try

            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/billing/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim requestMessage As String = ""
            Dim returnMessage As String = ""
            Dim paymentMethod As String = "E-Pay"

            Dim creditNoteJsonSource As String = ""
            Dim creditNoteMessageDataSet As New DataSet
            Dim stringReader As IO.StringReader


            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                creditNoteJsonSource = inquireInvoice(userId, password, entity, invoiceNumber)
                Dim doc As New System.Xml.XmlDocument
                doc = JsonConvert.DeserializeXmlNode(creditNoteJsonSource, "root")
                stringReader = New IO.StringReader(doc.InnerXml)
                creditNoteMessageDataSet.ReadXml(stringReader)

                Dim client = New RestClient(invoiceURL)

                client.Timeout = timeout
                client.ReadWriteTimeout = timeout

                Dim request = New RestRequest("invoices/settle-invoice", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("z.sessionToken", token)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("cache-control", "no-cache")


                Dim paymentItemMobile As New PaymentItemMobile
                Dim paymentObjectMobile As New PaymentMobileObject
                paymentObjectMobile.code = invoiceNumber
                paymentObjectMobile.payments = New List(Of PaymentItemMobile)

                With paymentItemMobile
                    .paymentMethod = paymentMethod
                    .currency = paymentCurrency
                    .exchangeRate = exchangeRate
                    .amount = amount
                    .bankAccount = bankAccount
                    .paymentReference = ePaymentReference
                    .bankTransReference = bankTransReference
                    .payByFlexField1 = subscriberName
                    .payByFlexField2 = subscriberMobile
                    .paymentTm = paymentTime
                End With

                paymentObjectMobile.payments.Add(paymentItemMobile)

                For Each rowCredit As DataRow In creditNoteMessageDataSet.Tables("creditNotes").Rows
                    paymentItemMobile = New PaymentItemMobile
                    With paymentItemMobile
                        .paymentMethod = "Credit Note"
                        .currency = rowCredit.Item("billCurrency")
                        .amount = Math.Abs(CDbl(rowCredit.Item("creditAmount")))
                        .bankAccount = ""
                        .paymentReference = rowCredit.Item("creditNoteNbr")
                        .paymentTm = paymentTime
                        .exchangeRate = exchangeRate
                    End With
                    paymentObjectMobile.payments.Add(paymentItemMobile)
                Next


                requestMessage = JsonConvert.SerializeObject(paymentObjectMobile)
                request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)
                returnMessage = response.Content

            End If

            Return returnMessage

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul payInvoiceMobile fatal error. Parameters: " & "{userId, password, entity, invoiceNumber, paymentCurrency, exchangeRate, amount, bankAccount, ePaymentReference,
                               bankTransReference, subscriberName, subscriberMobile, paymentTime}, Values: " &
                                                 "{" & userId & "," & password & "," & entity & "," & invoiceNumber & "," &
                                                 paymentCurrency & "," & exchangeRate & "," & amount & "," & bankTransReference & "," & subscriberName & "," & subscriberMobile & "," & paymentTime &
                                                    "}", ex)
            Throw ex

        End Try



    End Function

    '
    ' releaseBOLContainer
    '
    ' Classes
    ' Methods

    Public Class ReleaseContainerItem

        Public Property eqpNbr() As String
        Public Property releaseRefNbr() As String
        Public Property qty() As Integer
        Public Property time() As String
        Public Property user() As String

    End Class



    Public Function releaseBOLContainer(equipmentNumber As String, customsReleaseNo As String,
                                        quantity As Integer, customsReleaseDateTime As String,
                                        userId As String, password As String, entity As String) As String

        Dim returnMessage As String = ""

        Try

            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/bols/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim requestMessage As String = ""



            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                Dim client = New RestClient(invoiceURL)

                client.Timeout = timeout
                client.ReadWriteTimeout = timeout

                Dim request = New RestRequest("batch-release-by-cntr", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("z.sessionToken", token)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("cache-control", "no-cache")


                Dim containerItem As New ReleaseContainerItem

                With containerItem
                    .eqpNbr = equipmentNumber
                    .releaseRefNbr = customsReleaseNo
                    .qty = quantity
                    .time = customsReleaseDateTime
                    .user = userId
                End With

                requestMessage = JsonConvert.SerializeObject(containerItem)
                request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)

                returnMessage = response.Content

                Global_asax.logger.Info("RESTFul releaseBOLContainer debug info. Parameters: " & "{equipmentNumber, customsReleaseNo, quantity, customsReleaseDateTime, userId, password, entity}, Values: " &
                                     "{" & equipmentNumber & "," & customsReleaseNo & "," & quantity & "," & customsReleaseDateTime &
                                     userId & "," & password & "," & entity & "} Response: " & returnMessage)

            End If

            Return returnMessage

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul releaseBOLContainer fatal error. Parameters: " & "{equipmentNumber, customsReleaseNo, quantity, customsReleaseDateTime, userId, password, entity}, Values: " &
                                                 "{" & equipmentNumber & "," & customsReleaseNo & "," & quantity & "," & customsReleaseDateTime &
                                                 userId & "," & password & "," & entity & "}", ex)

            returnMessage = ex.Message

        End Try

        Return returnMessage

    End Function


    '
    ' Container booking message
    '

    Public Function customBookingContainer(containerId As String, plannedLocation As String,
                                        bookingNo As String, bookingDate As String,
                                        docNo As String, docValidityDate As String,
                                          userId As String, password As String, entity As String) As String

        Dim requestMessage As String = ""
        Dim returnMessage As String = ""
        Dim dataSetMessage As New DataSet
        Dim tableContainerInfo As New DataTable

        Try

            Dim invoiceURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/equipment/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)

            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                If isExistHoldCode(containerId, "AH", userId, password, entity) Then

                    Dim client = New RestClient(invoiceURL)

                    client.Timeout = timeout
                    client.ReadWriteTimeout = timeout

                    Dim request = New RestRequest("update", Method.POST)
                    request.RequestFormat = DataFormat.Json

                    request.AddHeader("z.sessionToken", token)
                    request.AddHeader("z.bizunitcode", entity)
                    request.AddHeader("z.user", userId)
                    request.AddHeader("z.ip", helper.getIPAddress)
                    request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                    request.AddHeader("cache-control", "no-cache")

                    Dim jsonUpdateBooking = helper.readSoapMessageTemplate("RESTMessage/UpdateBooking.json")

                    bookingDate = bookingDate.Replace("-", "")
                    bookingDate = bookingDate.Replace(":", "")
                    bookingDate = bookingDate.Replace("am", "")
                    bookingDate = bookingDate.Replace("AM", "")
                    bookingDate = bookingDate.Replace("pm", "")
                    bookingDate = bookingDate.Replace("PM", "")
                    bookingDate = bookingDate.Replace(" ", "")
                    bookingDate = bookingDate & "00"

                    docValidityDate = docValidityDate.Replace("-", "")
                    docValidityDate = Left(docValidityDate, 8)
                    docValidityDate = docValidityDate & "000000"

                    jsonUpdateBooking = jsonUpdateBooking.Replace("@containerId", containerId)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@plannedLocation", plannedLocation)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@customBookingNo", bookingNo)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@customBookingDate", bookingDate)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@doNumber", docNo)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@doValidity", docValidityDate)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@LHRemarks", "REMLH*" & docNo & "*" & docValidityDate & "*" & containerId)
                    jsonUpdateBooking = jsonUpdateBooking.Replace("@AHRemarks", "REMAH*" & bookingNo & "*" & plannedLocation & "*" & containerId)

                    requestMessage = jsonUpdateBooking
                    request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                    Dim response As IRestResponse = client.Execute(request)

                    returnMessage = response.Content

                    Global_asax.logger.Info("RESTFul customBookingContainer debug info. Parameters: " &
                                            "{containerId, plannedLocation, bookingNo, bookingDate, docNo, docValidityDate, userId, password, entity}, 
                                                Values: " &
                                            "{" & containerId & "," & plannedLocation & "," & bookingNo & "," & bookingDate & "," & docNo &
                                            "," & docValidityDate & "," & userId & "," & password & "," & entity & "} Response: " & returnMessage)

                    'todo: here check internal status
                    dataSetMessage = util.readDataFromJson(returnMessage)
                    tableContainerInfo = dataSetMessage.Tables("result")

                    If tableContainerInfo.Rows(0).Item("resultCode") = "1" Then
                        returnMessage = "CUST_BKG_SUCCESS"
                    Else
                        returnMessage = "CUST_BKG_FAILED"
                    End If

                Else

                    returnMessage = "HOLD_NOT_EXIST"

                End If

            End If

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul customBookingContainer fatal error. Parameters: " &
                                     "{containerId, plannedLocation, bookingNo, bookingDate, docNo, docValidityDate, remarks, userId, password, entity}, 
                                                Values: " &
                                     "{" & containerId & "," & plannedLocation & "," & bookingNo & "," & bookingDate & "," & docNo & "," &
                                     docValidityDate & "," & userId & "," & password & "," & entity & "}", ex)

            returnMessage = "CUST_BKG_ERROR_SEE_LOGS - " & ex.Message

        End Try

        Return returnMessage

    End Function


    Public Function customReleaseContainer(containerId As String, releaseReferenceNumber As String,
                                        remarks As String, holdAction As String, holdCode As String,
                                          userId As String, password As String, entity As String) As String

        Dim requestMessage As String = ""
        Dim returnMessage As String = ""
        Dim dataSetMessage As New DataSet
        Dim tableContainerInfo As New DataTable

        Try

            Dim updateURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/equipment/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)


            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                If isExistHoldCode(containerId, "CU", userId, password, entity) Then

                    Dim client = New RestClient(updateURL)

                    client.Timeout = timeout
                    client.ReadWriteTimeout = timeout

                    Dim request = New RestRequest("update", Method.POST)
                    request.RequestFormat = DataFormat.Json

                    request.AddHeader("z.reference", token)
                    request.AddHeader("z.user", userId)
                    request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                    request.AddHeader("cache-control", "no-cache")

                    Dim jsonUpdateRelease = helper.readSoapMessageTemplate("RESTMessage/UpdateRelease.json")
                    jsonUpdateRelease = jsonUpdateRelease.Replace("@containerId", containerId)
                    jsonUpdateRelease = jsonUpdateRelease.Replace("@releaseReferenceNum", releaseReferenceNumber)
                    jsonUpdateRelease = jsonUpdateRelease.Replace("@holdAction", holdAction)
                    jsonUpdateRelease = jsonUpdateRelease.Replace("@holdCode", holdCode)
                    jsonUpdateRelease = jsonUpdateRelease.Replace("@remarks", remarks)

                    requestMessage = jsonUpdateRelease
                    request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                    Dim response As IRestResponse = client.Execute(request)

                    returnMessage = response.Content

                    Global_asax.logger.Info("RESTFul customReleaseContainer debug info. Parameters: " &
                             "{containerId, releaseReferenceNumber, remarks, holdAction, holdCode, userId, password, entity}, 
                                      Values: " &
                              "{" & containerId & "," & releaseReferenceNumber & "," & remarks & "," & holdAction & "," & holdCode &
                              "," & userId & "," & password & "," & entity & "} Response: " & returnMessage)

                    'todo: here check internal status
                    dataSetMessage = util.readDataFromJson(returnMessage)
                    tableContainerInfo = dataSetMessage.Tables("result")

                    If tableContainerInfo.Rows(0).Item("resultCode") = "1" Then
                        returnMessage = "CUST_RELEASE_SUCCESS"
                    Else
                        returnMessage = "CUST_RELEASE_FAILED"
                    End If

                Else

                    returnMessage = "HOLD_NOT_EXIST"

                End If

            End If

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul customReleaseContainer fatal error. Parameters: " &
                                     "{containerId, releaseReferenceNumber, remarks, holdAction, holdCode, userId, password, entity}, 
                                      Values: " &
                                      "{" & containerId & "," & releaseReferenceNumber & "," & remarks & "," & holdAction & "," & holdCode &
                                      "," & userId & "," & password & "," & entity & "}", ex)

            returnMessage = "CUST_RELEASE_ERROR_SEE_LOGS - " & ex.Message

        End Try

        Return returnMessage

    End Function

    '
    ' Find hold codes by container Id
    '

    Private Function isExistHoldCode(containerId As String, holdCode As String,
                                      userId As String, password As String, entity As String) As Boolean

        Dim returnMessage As String = ""
        Dim isExisting As Boolean = False


        Try

            Dim customFindURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/equipment/"
            Dim token As String = zodiacSoapController.getAuthToken(userId, password, entity)
            Dim requestMessage As String = ""

            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                Dim client = New RestClient(customFindURL)
                Dim request = New RestRequest("retrieve", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("z.sessionToken", token)
                request.AddHeader("z.bizunitcode", entity)
                request.AddHeader("z.userid", userId)
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("cache-control", "no-cache")

                Dim jsonFindContainer = helper.readSoapMessageTemplate("RESTMessage/FindHoldCodes.json")
                jsonFindContainer = jsonFindContainer.Replace("@containerId", containerId)
                requestMessage = jsonFindContainer
                request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)
                returnMessage = response.Content

                'serialize and compare the hold code required
                Dim dataSetMessage As DataSet
                Dim tableContainerInfo As DataTable
                Dim holdCodes As String()

                dataSetMessage = util.readDataFromJson(returnMessage)
                tableContainerInfo = dataSetMessage.Tables("cntrInfo")
                holdCodes = tableContainerInfo.Rows(0)("holdCodes").Split(",")

                If holdCodes.Contains(holdCode) Then

                    Global_asax.logger.Info("isExistHoldCode: Hold code 'AH' exists. Parameters: " &
                                                            "{containerId},  Values: " & "{" & containerId & "}" & returnMessage)
                    isExisting = True
                Else

                    Global_asax.logger.Info("isExistHoldCode: Hold code 'AH' does not exist. Parameters: " &
                                                           "{containerId},  Values: " & "{" & containerId & "}" & returnMessage)
                    isExisting = False
                End If


            End If

            Return isExisting

        Catch ex As Exception

            Global_asax.logger.Fatal("isExistHoldCode fatal error. Parameters: " & "{containerId},  Values: " & "{" & containerId & "}", ex)
            returnMessage = ex.Message

        End Try

        Return isExisting

    End Function


    '
    ' inquiryGetContainerDetail
    '

    Public Function inquiryGetContainerDetail(containerId As String) As String

        Dim returnMessage As String = ""
        Dim zUserId As String = ""
        Dim zPassword As String = ""
        Dim zEntity As String = ""

        Try

            zUserId = ConfigurationManager.AppSettings("zodiac_user_id")
            zPassword = ConfigurationManager.AppSettings("zodiac_password")
            zEntity = ConfigurationManager.AppSettings("zodiac_entity")

            Dim customFindURL As String = ConfigurationManager.AppSettings("zodiac_RESTFUL_url") & "/equipment/"
            Dim token As String = zodiacSoapController.getAuthToken(zUserId, zPassword, zEntity)
            Dim requestMessage As String = ""

            If integrationController.isDataValidated(token, "zodiac_regex_valid_token_guid") Then

                Dim client = New RestClient(customFindURL)
                Dim request = New RestRequest("retrieve", Method.POST)
                request.RequestFormat = DataFormat.Json

                request.AddHeader("z.sessionToken", token)
                request.AddHeader("z.bizunitcode", zEntity)
                request.AddHeader("z.userid", zUserId)
                request.AddHeader("z.ip", helper.getIPAddress)
                request.AddHeader("z.locale", ConfigurationManager.AppSettings("zodiac_locale"))
                request.AddHeader("cache-control", "no-cache")

                Dim jsonFindContainer = helper.readSoapMessageTemplate("../RESTMessage/FindContainerDetail.json")
                jsonFindContainer = jsonFindContainer.Replace("@containerId", containerId)
                requestMessage = jsonFindContainer
                request.AddParameter("application/json", requestMessage, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(request)
                returnMessage = response.Content

                client = Nothing
                request = Nothing
                response = Nothing

                Global_asax.logger.Info("RESTFul inquiryGetContainerDetail info. Parameters: " &
                                        "{containerId},  Values: " & "{" & containerId & "}" & returnMessage)

            End If

        Catch ex As Exception

            Global_asax.logger.Fatal("RESTFul customFindContainer fatal error. Parameters: " & "{containerId},  Values: " & "{" & containerId & "}", ex)

            returnMessage = ex.Message

        End Try

        Return returnMessage

    End Function


End Class
