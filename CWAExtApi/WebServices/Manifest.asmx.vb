﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Manifest
    Inherits System.Web.Services.WebService

    '
    ' Post Manifest Bill Of Lading
    '
    <WebMethod(Description:="Post BOL object, returns XML string")>
    Public Function PostManifestBillOfLading(bolNbr As String, vesselVisitCode As String,
                             consignee As String, notifier As String, dst As String,
                             cargoType As String, loadStatus As String, tradeMode As String,
                             org As String, pol As String, pod As String, bolDataSet As DataSet) As String

        Dim returnXMLString As String = ""

        Try

            Dim manifestController As New ManifestController
            Dim retmsg As DataTable
            Dim returnXMLWriter As New System.IO.StringWriter
            retmsg = manifestController.postManifestBillOfLading(bolNbr, vesselVisitCode, consignee, notifier, dst, cargoType, loadStatus, tradeMode, org, pol, pod, bolDataSet)
            retmsg.WriteXml(returnXMLWriter, XmlWriteMode.IgnoreSchema, False)
            returnXMLString = returnXMLWriter.ToString

            manifestController = Nothing
            retmsg = Nothing
            returnXMLWriter = Nothing

            Global_asax.logger.Info("PostManifestBillOfLading successful. Parameters: {bol}, Values: {" & bolNbr & "}, ReturnMessage: " & returnXMLString)

        Catch ex As Exception
            returnXMLString = ex.ToString
            Global_asax.logger.Error("PostManifestBillOfLading error. Parameters: {bol}, Values: {" & bolNbr & "}, ReturnMessage: " & returnXMLString)
        End Try

        Return returnXMLString

    End Function


    <WebMethod(Description:="Get list of vessel schedules for EMDS manifest, returns XML string")>
    Public Function GetEMDSVesselVisit() As String

        Dim manifest As New ManifestController
        Dim tableVesselSchedules As DataTable
        Dim xmlResult As String = ""

        Try
            Dim returnXMLWriter As New System.IO.StringWriter
            tableVesselSchedules = manifest.getVesselSchedule()
            tableVesselSchedules.TableName = "GetEMDSVesselVisit"
            tableVesselSchedules.WriteXml(returnXMLWriter, XmlWriteMode.IgnoreSchema, False)
            xmlResult = returnXMLWriter.ToString
        Catch ex As Exception
            Global_asax.logger.Error("GetEMDSVesselVisit error. Parameters: None}", ex)
            xmlResult = ex.Message
        End Try
        Return xmlResult
    End Function

End Class