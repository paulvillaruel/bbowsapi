﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="https://services.dpworldberbera.com/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class PaymentInterface
    Inherits System.Web.Services.WebService

    Public zodiacSoap As New ZodiacSoapController
    Public zodiacRestSharp As New RestSharpZodiacController


    <WebMethod(Description:="Zodiac test token")>
    Public Function GetToken(userId As String, password As String, entityName As String) As String
        Dim token As String = ""
        Try
            token = zodiacSoap.getAuthToken(userId, password, entityName)

            Global_asax.logger.Info("Get token successful. Parameters: " & "{userId, password, entityName}, Values: " &
                                                      "{" & userId & "," & password & "," & entityName & "}, " &
                                                       "ReturnToken: " & token)


        Catch ex As Exception
            token = ex.ToString
            Global_asax.logger.Error("Get token error. Parameters: " & "{userId, password, entityName}, Values: " &
                                                      "{" & userId & "," & password & "," & entityName & "}, " &
                                                       "ReturnMessage: " & token, ex)

        End Try
        Return token
    End Function


    <WebMethod(Description:="Invoice inquiry message API")>
    Public Function InquireInvoice(userId As String, password As String, entityName As String, invoiceNumber As String) As String

        Dim inquireInvoiceResult As String = ""
        Try

            inquireInvoiceResult = zodiacRestSharp.inquireInvoice(userId, password, entityName, invoiceNumber)

            Global_asax.logger.Info("Inquire invoice successful. Parameters: " & "{userId, password, entityName, invoiceNumber}, Values: " &
                                                      "{" & userId & "," & password & "," & entityName & "," & invoiceNumber & "}, " &
                                                       "ReturnJSON: " & inquireInvoiceResult)


        Catch ex As Exception
            inquireInvoiceResult = ex.ToString

            Global_asax.logger.Error("Inquire invoice error. Parameters: " & "{userId, password, entityName, invoiceNumber}, Values: " &
                                                      "{" & userId & "," & password & "," & entityName & "," & invoiceNumber & "}", ex)

        End Try

        Return inquireInvoiceResult

    End Function


    <WebMethod(Description:="Invoice payment message API")>
    Public Function PayInvoice(userId As String, password As String, entity As String,
                               invoiceNumber As String, paymentMethod As String, paymentCurrency As String,
                               exchangeRate As Double, amount As Double, bankAccount As String, paymentReference As String,
                                paymentTime As String) As String

        Dim payInvoiceResult As String = ""

        Try

            payInvoiceResult = zodiacRestSharp.payInvoice(userId, password, entity, invoiceNumber,
                                          paymentMethod, paymentCurrency, exchangeRate, amount, bankAccount, paymentReference, paymentTime)

            Global_asax.logger.Info("Pay invoice successful. Parameters: " & "{userId, password, entity, invoiceNumber,
                                          paymentMethod, paymentCurrency, exchangeRate, amount, bankAccount, paymentReference, paymentTime}, Values: " &
                                          "{" & userId & "," & password & "," & entity & "," & invoiceNumber & "," &
                                          paymentMethod & "," & paymentCurrency & "," & exchangeRate & "," & amount & "," &
                                          "{" & bankAccount & "," & paymentReference & "," & paymentTime & "}, " &
                                           "ReturnJSON: " & payInvoiceResult)

        Catch ex As Exception

            payInvoiceResult = ex.ToString

            Global_asax.logger.Error("Pay invoice error. Parameters: " & "{userId, password, entity, invoiceNumber,
                                          paymentMethod, paymentCurrency, exchangeRate, amount, bankAccount, paymentReference, paymentTime}, Values: " &
                                          "{" & userId & "," & password & "," & entity & "," & invoiceNumber & "," &
                                          paymentMethod & "," & paymentCurrency & "," & exchangeRate & "," & amount & "," &
                                          "{" & bankAccount & "," & paymentReference & "," & paymentTime & "}", ex)

        End Try

        Return payInvoiceResult

    End Function


    <WebMethod(Description:="Invoice payment mobile message API")>
    Public Function PayInvoiceMobile(userId As String, password As String, entity As String,
                               invoiceNumber As String, paymentMethod As String, paymentCurrency As String,
                               exchangeRate As Double, amount As Double, bankAccount As String, epaymentReference As String,
                                subscriberName As String, subscriberMobile As String, bankTransReference As String, paymentTime As String) As String

        Dim payInvoiceMobileResult As String = ""

        Try

            payInvoiceMobileResult = zodiacRestSharp.payInvoiceMobile(userId, password, entity, invoiceNumber, paymentCurrency, exchangeRate, amount,
                                                                      bankAccount, epaymentReference, bankTransReference, subscriberName, subscriberMobile, paymentTime)

            Global_asax.logger.Info("Pay invoice mobile successful. Parameters: " & "{userId, password, entity, invoiceNumber, paymentCurrency, exchangeRate, amount,
                                          bankAccount, epaymentReference, bankTransReference, subscriberName, subscriberMobile, paymentTime}, Values: " &
                                          "{" & userId & "," & password & "," & entity & "," & invoiceNumber & "," &
                                         paymentCurrency & "," & exchangeRate & "," & amount & "," & bankAccount & "," &
                                           epaymentReference & "," & bankTransReference & "," & subscriberName & "," & subscriberMobile & "," & paymentTime & "}" &
                                           "ReturnJSON: " & payInvoiceMobileResult)

        Catch ex As Exception

            payInvoiceMobileResult = ex.ToString

            Global_asax.logger.Error("Pay invoice mobile error. Parameters: " & "{userId, password, entity, invoiceNumber, paymentCurrency, exchangeRate, amount,
                                          bankAccount, epaymentReference, bankTransReference, subscriberName, subscriberMobile, paymentTime}, Values: " &
                                          "{" & userId & "," & password & "," & entity & "," & invoiceNumber & "," &
                                         paymentCurrency & "," & exchangeRate & "," & amount & "," & bankAccount & "," &
                                           epaymentReference & "," & bankTransReference & "," & subscriberName & "," & subscriberMobile & "," & paymentTime & "}" &
                                           "ReturnJSON: " & payInvoiceMobileResult, ex)

        End Try

        Return payInvoiceMobileResult

    End Function


End Class