﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Enquiry
    Inherits System.Web.Services.WebService


    <WebMethod(Description:="Get list of vessel schedules on a window, returns XML string")>
    Public Function GetVesselVisit() As String

        Dim inquiryController As New InquiryController
        Dim returnXMLWriter As New System.IO.StringWriter
        Dim tableVesselSchedules As DataTable
        Dim xmlResult As String = ""

        Dim boolGetVesselSchedulesPTS As Boolean = False
        boolGetVesselSchedulesPTS = CBool(ConfigurationManager.AppSettings("pts_allow_get_gc_vessel_visit"))

        Try

            tableVesselSchedules = inquiryController.getVesselSchedule()

            If boolGetVesselSchedulesPTS = True Then
                Dim tableVesselSchedulesPTS As DataTable
                tableVesselSchedulesPTS = inquiryController.getVesselSchedulePTS
                tableVesselSchedules.Merge(tableVesselSchedulesPTS)
            End If

            tableVesselSchedules.TableName = "GetVesselVisit"
            tableVesselSchedules.WriteXml(returnXMLWriter, XmlWriteMode.IgnoreSchema, False)
            xmlResult = returnXMLWriter.ToString
            Global_asax.logger.Info(xmlResult)
        Catch ex As Exception
            Global_asax.logger.Error(ex.Message, ex)
            xmlResult = ex.Message
        End Try
        Return xmlResult
    End Function


    '
    ' Get container list by BOL
    '
    <WebMethod(Description:="Get list of container by BOL, returns XML string")>
    Public Function GetContainerListByBOL(bol As String) As String

        Dim returnXMLString As String = ""

        Try

            Dim inquiryController As New InquiryController
            Dim retmsg As DataTable
            Dim returnXMLWriter As New System.IO.StringWriter
            retmsg = inquiryController.inquireContainerList("BOL", bol)
            retmsg.WriteXml(returnXMLWriter, XmlWriteMode.IgnoreSchema, False)

            returnXMLString = returnXMLWriter.ToString

            inquiryController = Nothing
            retmsg = Nothing
            returnXMLWriter = Nothing

            Global_asax.logger.Info(returnXMLString)

        Catch ex As Exception
            returnXMLString = ex.ToString
            Global_asax.logger.Error(ex.Message, ex)
        End Try

        Return returnXMLString

    End Function


    '
    ' Get container list by CNT
    '
    <WebMethod(Description:="Get list of container by container number, returns XML string")>
    Public Function GetContainerListByContainerId(containerId As String) As String

        Dim returnXMLString As String = ""

        Try

            Dim inquiryController As New InquiryController
            Dim retmsg As DataTable
            Dim returnXMLWriter As New System.IO.StringWriter
            retmsg = inquiryController.inquireContainerList("CNT", containerId)
            retmsg.WriteXml(returnXMLWriter, XmlWriteMode.IgnoreSchema, False)

            returnXMLString = returnXMLWriter.ToString

            inquiryController = Nothing
            retmsg = Nothing
            returnXMLWriter = Nothing
            Global_asax.logger.Info(returnXMLString)

        Catch ex As Exception
            returnXMLString = ex.ToString
            Global_asax.logger.Error(ex.Message, ex)
        End Try

        Return returnXMLString

    End Function


    '
    ' Get container detail
    '
    <WebMethod(Description:="Get detail of container by container number, returns XML string")>
    Public Function GetContainerDetailByContainerId(containerId As String) As System.Data.DataSet

        Dim returnXMLString As String = ""
        Dim dsContainerDetail As New DataSet
        Dim dtContainerDetail As DataTable
        Dim tableContainerDetail As New DataTable
        Dim inquiryController As New InquiryController
        Dim returnXMLWriter As New System.IO.StringWriter
        Try
            tableContainerDetail = inquiryController.inquireContainerDetail(containerId)
            tableContainerDetail.WriteXml(returnXMLWriter, XmlWriteMode.IgnoreSchema, False)
            returnXMLString = returnXMLWriter.ToString
            Global_asax.logger.Info("GetContainerDetailByContainerId successful. Parameters: {containerId}, Values: {" & containerId & "}, ReturnMessage: " & returnXMLString)

        Catch ex As Exception
            tableContainerDetail = inquiryController.returnErrorAsDataTable(ex.Message)
            Global_asax.logger.Error("GetContainerDetailByContainerId error. Parameters: {containerId}, Values: {" & containerId & "}, ReturnMessage: " & ex.Message)
        Finally
            inquiryController = Nothing
        End Try

        tableContainerDetail.TableName = "GetContainerDetailByContainerId"
        dtContainerDetail = tableContainerDetail.Copy()
        dsContainerDetail.Tables.Add(dtContainerDetail)

        Return dsContainerDetail

    End Function

End Class