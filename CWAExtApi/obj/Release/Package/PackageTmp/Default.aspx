﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CWAExtApiWS._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
        .style2
        {
            width: 100%;
            border-style: solid;
            border-width: 1px;
        }
    </style>
</head>
<body>
    <p>
        <b>DP World Berbera - CWAExtApi</b></p>
    <p>
        App Name: CWAExtApi<br />
        Version: 1.0 build 25 Feb 2019</p>
    <p class="style1">
        Release Notes</p>
    <table class="style2">
        <tr>
            <td>
                Version 1.0.0</td>
            <td>
                Vessel and container inquiry</td>
            <td>
                Feb 13, 2019</td>
        </tr>   
                <tr>
            <td>
                Version 1.0.1</td>
            <td>
                Add PostManifestBillOfLading web method</td>
            <td>
                Feb 24, 2019</td>
        </tr>   
        <tr>
            <td>
                Version 1.0.2</td>
            <td>
                Added GetVesselVisitEMDS web method</td>
            <td>
                Feb 25, 2019</td>
        </tr>      
    </table>
    <p>
        &nbsp;</p>
    <p class="style1">
        Support Information</p>
    <p>
        Paul Jeremy N Villaruel<br />
        Skype: pvillaruel@gmail.com<br />
        WhatsApp: +252633359657<br />        
    </p>
</body>
</html>
