﻿Imports System.Data
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.Net
Imports Newtonsoft
Imports Newtonsoft.Json

Public Class IntegrationHelper

    Public Function readSoapMessageTemplate(xmlFilePath As String) As String
        Try
            Dim path As String = System.Web.HttpContext.Current.Server.MapPath(xmlFilePath)
            Dim reader As IO.StreamReader = File.OpenText(path)
            Dim messageTemplate As String = reader.ReadToEnd()
            Return messageTemplate
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    Public Function readJsonMessageTemplate(jsonFilePath As String) As String
        Try
            Dim path As String = System.Web.HttpContext.Current.Server.MapPath(jsonFilePath)
            Dim reader As IO.StreamReader = File.OpenText(path)
            Dim messageTemplate As String = reader.ReadToEnd()
            Return messageTemplate
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    Public Function getIPAddress(local As Boolean) As String
        Dim context As System.Web.HttpContext = System.Web.HttpContext.Current
        Dim sIPAddress As String = context.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If String.IsNullOrEmpty(sIPAddress) Then
            Return context.Request.ServerVariables("REMOTE_ADDR")
        Else
            Dim ipArray As String() = sIPAddress.Split(New [Char]() {","c})
            Return ipArray(0)
        End If
    End Function

    Public Function getIPAddress() As String
        'Dim Host As Net.IPHostEntry
        'Dim Hostname As String
        'Dim IPAddress As String = ""
        'Hostname = My.Computer.Name
        'Host = Dns.GetHostEntry(Hostname)
        'For Each IP As IPAddress In Host.AddressList
        '    If IP.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
        '        IPAddress = Convert.ToString(IP)
        '    End If
        'Next
        'Return IPAddress
        Return "127.0.0.1"
    End Function


    Public Function deserializeJson(jsonData As String) As DataTable
        Dim table = JsonConvert.DeserializeObject(Of DataTable)(jsonData)
        Return table
    End Function

End Class
