﻿Imports System.Xml
Imports Newtonsoft.Json

Public Class Utils

    Public Function convertDateTime(dateInput As String, formatInput As String) As String
        Try
            Dim dateValue As Date
            dateValue = Date.Parse(dateInput)
            Return dateValue.ToString(formatInput)
        Catch ex As Exception
            Global_asax.logger.Error("Invalid date. Parameters: " & "{dateInput, formatInput}, Values: " &
                                                      "{" & dateInput & "," & formatInput & "}", ex)

        End Try
    End Function

    Public Function readDataFromJson(ByVal jsonString As String, ByVal Optional mode As XmlReadMode = XmlReadMode.Auto) As DataSet
        jsonString = "{ ""rootNode"": {" & jsonString.Trim().TrimStart("{"c).TrimEnd("}"c) & "} }"
        Dim xd = JsonConvert.DeserializeXmlNode(jsonString)
        Dim result = New DataSet()
        result.ReadXml(New XmlNodeReader(xd), mode)
        Return result
    End Function


End Class
